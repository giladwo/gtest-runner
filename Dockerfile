FROM fedora:36

RUN dnf upgrade --assumeyes && \
    dnf install --assumeyes git gcc clang clang-tools-extra cmake ninja-build valgrind diffutils tree bat gdb && \
    dnf autoremove --assumeyes
